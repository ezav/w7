$(document).ready(function(){
    $('.sl').slick({
			dots: true,
        infinite: true,
			speed: 1000,
        slidesToShow: 4,
		
		
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
				
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 440,
            settings: {
				
              slidesToShow: 1,
              slidesToScroll: 1
            }
			}
     
        ]
    });
    $(".sl").on('afterChange', function(event, slick, currentSlide){
				$("#cp").text(currentSlide + 1)
		});
  });
  